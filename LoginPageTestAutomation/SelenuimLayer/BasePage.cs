﻿using System;
using OpenQA.Selenium;

namespace SeleniumLayer
{
    /// <summary>
    /// Basis for all kind of pages.
    /// </summary>
    public class BasePage
    {
        /// <summary>
        /// Selenium WebDriver
        /// </summary>
        protected IWebDriver _driver;

        protected BasePage(string title, IWebDriver driver)
        {
            // Check that we create right page with correct url.
            if (driver.Title.Contains(title))
            {
                _driver = driver;
            }
            else
            {
                throw new Exception($"It is wrong page for {title} page. Check URL: {driver.Url}");
            }
        }
    }
}
