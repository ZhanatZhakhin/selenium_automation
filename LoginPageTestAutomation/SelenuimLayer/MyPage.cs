﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace SeleniumLayer
{
    /// <summary>
    /// This class encapsulate all methods that are necessary for "My page".
    /// </summary>
    public class MyPage : BasePage
    {
        /// <summary>
        /// Used for page validation.
        /// </summary>
        private const string Title = "マイページ";
        /// <summary>
        /// Using WebDriver create an instance of MyPage.
        /// </summary>
        /// <param name="driver"></param>
        public MyPage(IWebDriver driver) : base(Title, driver) { }
        
        /// <summary>
        /// Getting a text of header "h2" on "My page".
        /// </summary>
        /// <returns>String value</returns>
        public string GetHeaderText()
        {
            var header = _driver.FindElement(By.TagName("h2"));
            return header.Text;
        }
    }
}
