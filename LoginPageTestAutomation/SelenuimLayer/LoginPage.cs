﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace SeleniumLayer
{
    /// <summary>
    /// This class encapsulate all methods that are necessary for "Login page".
    /// </summary>
    public class LoginPage : BasePage
    {
        /// <summary>
        /// Used for page validation.
        /// </summary>
        private const string Title = "ログイン";

        public LoginPage(IWebDriver driver) : base(Title, driver) { }

        /// <summary>
        /// Using the input data, this method performs authorization actions.
        /// </summary>
        /// <param name="email">Email of user</param>
        /// <param name="password">Password</param>
        /// <param name="failAttempt">If False return an instance of MyPage.
        /// If True - null</param>
        /// <returns>An instance of MyPage or null</returns>
        public MyPage? LoginToMyPage(string email, string password, bool failAttempt)
        {
            var loginInput = _driver.FindElement(By.Id("email"));
            var passwordInput = _driver.FindElement(By.Id("password"));
            var loginButton = _driver.FindElement(By.Id("login-button"));
            loginInput.Clear();
            passwordInput.Clear();
            loginInput.SendKeys(email);
            passwordInput.SendKeys(password);
            loginButton.Click();
            if (!failAttempt)
            {
                return new MyPage(_driver);
            }
            return null;
        }
        /// <summary>
        /// Find a warning text from email field.
        /// </summary>
        /// <returns>Text of email-message element</returns>
        public string GetEmailErrorWarning()
        {
            var emailWarning = _driver.FindElement(By.Id("email-message"));
            return emailWarning.Text;
        }

        /// <summary>
        /// Find a warning text from password field.
        /// </summary>
        /// <returns>Text of password-message element</returns>
        public string GetPasswordWarning()
        {
            var passwordWarning = _driver.FindElement(By.Id("password-message"));
            return passwordWarning.Text;
        }

        /// <summary>
        /// This method enters the password to password field. Then make a attempt to copy/paste.
        /// </summary>
        /// <param name="password">Text for password</param>
        /// <returns>Copied value from email input field.</returns>
        public string GetPasswordFromField(string password)
        {
            var passwordInput = _driver.FindElement(By.Id("password"));
            passwordInput.Clear();
            passwordInput.SendKeys(password);
            Actions actionProvider = new Actions(_driver);
            IAction actionForCopy = actionProvider
                .MoveToElement(passwordInput)
                .Click()
                .KeyDown(Keys.Control)
                .SendKeys("a")
                .SendKeys("c")
                .Build();

            actionForCopy.Perform();
            var emailInput = _driver.FindElement(By.Id("email"));
            emailInput.Clear();
            IAction actionForPaste = actionProvider
                .MoveToElement(emailInput)
                .Click()
                .KeyDown(Keys.Control)
                .SendKeys("v")
                .Build();
            actionForPaste.Perform();

            return emailInput.GetAttribute("value");
        }

        /// <summary>
        /// This method enters the email to email field. Then make a attempt to copy/paste.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Copied value from email input field.</returns>
        public string GetEmailFromField(string email)
        {
            var emailInput = _driver.FindElement(By.Id("email"));
            emailInput.Clear();
            emailInput.SendKeys(email);
            Actions actionProvider = new Actions(_driver);
            IAction actionForCopy = actionProvider
                .MoveToElement(emailInput)
                .Click()
                .KeyDown(Keys.Control)
                .SendKeys("a")
                .SendKeys("c")
                .Build();
            actionForCopy.Perform();

            emailInput.Clear();

            IAction actionForPaste = actionProvider
                .MoveToElement(emailInput)
                .Click()
                .KeyDown(Keys.Control)
                .SendKeys("v")
                .Build();
            actionForPaste.Perform();

            return emailInput.GetAttribute("value");
        }

    }
}

