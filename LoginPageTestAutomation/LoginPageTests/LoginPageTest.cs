﻿using SeleniumLayer;
using Xunit;


namespace LoginPageTests
{
   
    public class LoginPageTest : BaseTest
    {
        
        private const PagesEnum Page = PagesEnum.LoginPage; 
        public LoginPageTest() : base(Page) { }
        
        [Theory]
        [InlineData("ichiro@example.com", "password")]
        [InlineData("sakura@example.com", "pass1234")]
        [InlineData("jun@example.com", "pa55w0rd!")]
        [InlineData("yoshiki@example.com", "pass-pass")]
        public void ShouldBeSuccessfulLogging(string email, string password)
        {
            var loginPage = new LoginPage(Driver);
            var myPage = loginPage.LoginToMyPage(email, password, false);
            
            Assert.Equal("マイページ", myPage?.GetHeaderText());
        }

        [Fact]
        public void ShouldGetErrorWithEmptyFields()
        {
            var loginPage = new LoginPage(Driver);
            _ = loginPage.LoginToMyPage("", "", true);
            Assert.Equal("このフィールドを入力してください。", loginPage.GetEmailErrorWarning());
            Assert.Equal("このフィールドを入力してください。", loginPage.GetPasswordWarning());
        }

        [Fact]
        public void ShouldBeErrorWithIncorrectData()
        {
            var loginPage = new LoginPage(Driver);
            _ = loginPage.LoginToMyPage("error@error.com", "error", true);
            Assert.Equal("メールアドレスまたはパスワードが違います。", loginPage.GetEmailErrorWarning());
            Assert.Equal("メールアドレスまたはパスワードが違います。", loginPage.GetPasswordWarning());
        }

        [Fact]
        public void ShouldBeErrorWithInvalidEmail()
        {
            var loginPage = new LoginPage(Driver);
            _ = loginPage.LoginToMyPage("invalid>>>@email.com", "pass", true);
            Assert.Equal("メールアドレスを入力してください。", loginPage.GetEmailErrorWarning());
        }
        [Fact]
        public void ShouldBeErrorWithCorrectEmailAndWrongPassword()
        {
            var loginPage = new LoginPage(Driver);
            _ = loginPage.LoginToMyPage("ichiro@example.com", "wrong_pass", true);
            Assert.Equal("メールアドレスまたはパスワードが違います。", loginPage.GetEmailErrorWarning());
            Assert.Equal("メールアドレスまたはパスワードが違います。", loginPage.GetPasswordWarning());
        }
        [Fact]
        public void ShouldBeErrorWithWrongEmailAndCorrectPassword()
        {
            var loginPage = new LoginPage(Driver);
            _ = loginPage.LoginToMyPage("ichiroERROR@example.com", "password", true);
            Assert.Equal("メールアドレスまたはパスワードが違います。", loginPage.GetEmailErrorWarning());
            Assert.Equal("メールアドレスまたはパスワードが違います。", loginPage.GetPasswordWarning());
        }

        [Fact]
        public void ShouldNotAbleGetTextFromPasswordField()
        {
            var loginPage = new LoginPage(Driver);
            
            Assert.NotEqual("CopyPass", loginPage.GetPasswordFromField("CopyPass"));
        }

        [Fact]
        public void ShouldAbleGetTextFromEmailField()
        {
            var loginPage = new LoginPage(Driver);
            Assert.Equal("test@test.com", loginPage.GetEmailFromField("test@test.com"));
        }


    }
}
