﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using OpenQA.Selenium;
using LoginPageTests.TestHelpers;
using OpenQA.Selenium.Chrome;

namespace LoginPageTests
{
    public enum PagesEnum
    {
        LoginPage,
        MyPage,
        SignupPage,
        TopPage
    }

    public abstract class BaseTest : IDisposable
    {
        protected IWebDriver Driver;
        protected readonly string Url;
        protected BaseTest(PagesEnum page)
        {
            IConfiguration config = TestHelper.GetConfiguration();
            Driver = new ChromeDriver();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Driver.Manage().Window.Size = new Size(1024, 768);
            switch (page)
            {
                case PagesEnum.LoginPage:
                    Url = config["loginPage"];
                    break;
                case PagesEnum.MyPage:
                    Url = config["myPage"];
                    break;
            }
            Driver.Navigate().GoToUrl(Url);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Driver.Manage().Cookies.DeleteAllCookies();
                Driver.Quit();
            }
        }

    }
}
